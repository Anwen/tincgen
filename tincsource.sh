#!/bin/bash


#apt-get install tinc
# bleeeeeeeeeeeeeeeeeeeeeeeeeeeeeee

case $1 in
"serveur")
;;
"client")
;;
*)
echo "serveur ou client?"
exit
;;
esac


echo
echo "configuration coté $1"
echo
echo

echo "nom du vpn?"
read vpnname
echo "nom du serveur?"
read servername
echo "IP du serveur (dans le tinc)"
read IPserver
echo "IP publique du serveur (62.210.205.221)"
read IP_pub_server
echo "port?"
read port

case $1 in
"client")

echo "nom du client?"
read clientname
echo "IP du client?"
read IPclient

;;
esac



case $1 in
"serveur")

cat >> /etc/tinc/$vpnname/tinc.conf << EOI
Name = $servername
AddressFamily = any
Interface = $vpnname
mode = switch 
EOI
echo "/etc/tinc/$vpnname/tinc.conf:\n" /etc/tinc/$vpnname/tinc.conf

cat >> /etc/tinc/$vpnname/hosts/$servername << EOI
#Address = 62.210.205.221
Address = $IP_pub_server
Subnet = $IPserver/32
Port = $port
EOI
echo "/etc/tinc/$vpnname/hosts/$servername:\n" /etc/tinc/$vpnname/hosts/$servername

cat >> /etc/tinc/$vpnname/tinc-up << EOI
#!/bin/sh
ip link set \$INTERFACE up
ip addr add $IPserver/24 dev \$INTERFACE

echo 1 >/proc/sys/net/ipv4/ip_forward
EOI
echo "/etc/tinc/$vpnname/tinc-up:\n" /etc/tinc/$vpnname/tinc-up

cat >> /etc/tinc/$vpnname/tinc-down << EOI
#!/bin/sh
ip addr flush dev \$INTERFACE
ip link set \$INTERFACE down 
EOI
echo "/etc/tinc/$vpnname/tinc-down:\n" /etc/tinc/$vpnname/tinc-down

;;
"client")


cat >> /etc/tinc/$vpnname/tinc.conf << EOI
Name = $clientname
AddressFamily = any
Interface = $vpnname
ConnectTo = $servername
mode = switch 
EOI
echo "/etc/tinc/$vpnname/tinc.conf:\n" /etc/tinc/$vpnname/tinc.conf

cat >> /etc/tinc/$vpnname/hosts/$clientname << EOI
#Address = 
Subnet = $IPclient/32
EOI
echo "/etc/tinc/$vpnname/hosts/$clientname:\n" /etc/tinc/$vpnname/hosts/$clientname

cat >> /etc/tinc/$vpnname/hosts/$servername << EOI
Address = $IP_pub_server
Subnet = $IPserver/32
Port = $port
EOI
echo "/etc/tinc/$vpnname/hosts/$servername:\n" /etc/tinc/$vpnname/hosts/$servername

cat >> /etc/tinc/$vpnname/tinc-up << EOI
#!/bin/sh
VPN_GATEWAY=$IPserver
IP_CLIENT=$IPclient/24
IP_SERVER=62.210.205.221
#IP_SERVER=$IP_pub_server
ORIGINAL_GATEWAY=\`ip route show | grep ^default | cut -d ' ' -f 3-5\`
ip link set \$INTERFACE up

ip a a \$IP_CLIENT dev \$INTERFACE
ip r a 0.0.0.0/1 via \$VPN_GATEWAY dev \$INTERFACE
ip r a 128.0.0.0/1 via \$VPN_GATEWAY dev \$INTERFACE
ip r a \$IP_SERVER via \$ORIGINAL_GATEWAY

EOI
echo "/etc/tinc/$vpnname/tinc-up:\n" /etc/tinc/$vpnname/tinc-up

cat >> /etc/tinc/$vpnname/tinc-down << EOI
#!/bin/sh
IP_SERVER=62.210.205.221
#IP_SERVER=$IP_pub_server
ORIGINAL_GATEWAY=\`ip route show | grep ^default | cut -d ' ' -f 3-5\`

ip addr flush dev \$INTERFACE
ip r d \$IP_SERVER via \$ORIGINAL_GATEWAY
ip link set \$INTERFACE down

EOI
echo "/etc/tinc/$vpnname/tinc-down:\n" /etc/tinc/$vpnname/tinc-down

;;
*)
echo "\$1=$1\n serveur ou client?"
exit
;;
esac

mkdir -p /etc/tinc/$vpnname/hosts

echo "création des clefs"
tincd -n $vpnname -K 4096

chmod 750 /etc/tinc/$vpnname/tinc-*
