#!/usr/bin/env zsh 

##############################
# Global Variables
# ================
#
# role (client/server)
# vpn_name
# server_name 
# server_tinc_IP (In the Tinc)
# server_public_IP (In teh wildz internetz)
# client_name (Yes, your machine's name.)
# client_IP (In the Tinc)
# port
# prefix
# INTERFACE
# VPN_GATEWAY (if you are a client)
# ORIGINAL_GATEWAY (internally used by Tinc)
# OS_PREFIX (/etc, /usr/local/etc, etc)

case $(uname) in
  Linux)   OS_PREFIX="/etc";;
  FreeBSD) OS_PREFIX="/usr/local/etc/";;
  Darwin)  OS_PREFIX="/usr/etc";;
  *) echo "OS not supported, this script only supports FreeBSD & Linux" ; exit 2
esac

##############################
# Functions
# =========
# 
# General Functions
# -----------------
#
# disp_baner()
# ask($1 $2)
#
# fetch_server_variables()
# config_server_batch()
# Linux-up-down-server()
# FreeBSD-up-down-server()
#
# fetch_client_variables()
# config_client_batch()
# Linux-up-down-client()
# FreeBSD-up-down-client()
#
# genfiles()
#
#
# Metafuctions
# ------------
# conf_server()
# conf_client()
# select_conf()

disp_banner()
{
    cat << EOF
  _______ _             _____                  _     
 |__   __(_)           / ____|                | |    
    | |   _ _ __   ___| |  __  ___ _ __    ___| |__  
    | |  | | '_ \ / __| | |_ |/ _ \ '_ \  / __| '_ \ 
    | |  | | | | | (__| |__| |  __/ | | |_\__ \ | | |
    |_|  |_|_| |_|\___|\_____|\___|_| |_(_)___/_| |_|
    
EOF
}

ask()
{
    print "[?] $1"
    read "$2"
}

# Asks for variables that will be used during the script
fetch_server_variables()
{

    ask "VPN Name?" vpn_name
    ask "Server Name ?" server_name
    ask "Server's IP (in the tinc)" server_tinc_IP
    ask "Server's public IP" server_public_IP 
    ask "prefix?" prefix
    ask "port?" port
}

# Writes the configuration on disk
config_server_batch()
{
cat << EOI  > ${OS_PREFIX}/tinc/${vpn_name}/tinc.conf
Name          = ${server_name}
AddressFamily = any
Interface     = ${vpn_name}
mode          = switch 
Cipher        = aes-256-cbc
Digest        = SHA512
Port          = ${port}

EOI

    print "${OS_PREFIX}/tinc/${vpn_name}/tinc.conf"

cat << EOI  >> ${OS_PREFIX}/tinc/${vpn_name}/hosts/${server_name}
Address = ${server_public_IP}
Subnet  = ${server_tinc_IP}/32
Port    = ${port}
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/hosts/${server_name}"

return 0
} # config_server_batch()

#### UP & DOWN SERVER Scripts ####

Darwin-up-down-server()
{
  cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-up
#!/bin/sh

ifconfig $INTERFACE up
ifconfig $INTERFACE ${server_tinc_IP}
ifconfig $INTERFACE mtu 1350

ifconfig \$INTERFACE inet ${server_tinc_IP} netmask 255.255.255.255
route add ${server_tinc_IP}/${prefix} -iface tap0

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-up"

cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-down
#!/bin/sh
ifconfig \$INTERFACE down
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-down"
} 

FreeBSD-up-down-server()
{
cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-up
#!/bin/sh
ifconfig \$INTERFACE up
ifconfig \$INTERFACE ${server_tinc_IP}

print 1 >/proc/sys/net/ipv4/ip_forward
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-up"

cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-down
#!/bin/sh
ifconfig \$INTERFACE down 
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-down"
  } 

Linux-up-down-server()
{
cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-up
#!/bin/sh
ip link set \$INTERFACE up
ip addr add ${server_tinc_IP}/${prefix} dev \$INTERFACE

print 1 >/proc/sys/net/ipv4/ip_forward
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-up"

cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-down
#!/bin/sh
ip addr flush dev \$INTERFACE
ip link set \$INTERFACE down 
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-down"
} 

fetch_client_variables()
{
    print "[*] Node Configuration"
    ask "VPN name?" vpn_name
    ask "Node name?" client_name
    ask "Client's IP address? (in Tinc)" client_IP
    ask "Network Prefix" prefix
    ask "Server's name?" server_name
    ask "Server's public IP" server_public_IP
    ask "Server's private IP (in Tinc)" server_tinc_IP 

    export vpn_name
    export client_name
    export client_IP
    export prefix
    export server_name
    export server_public_IP 
    export server_tinc_IP

}

config_client_batch()
{
cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc.conf
Name = ${client_name}
AddressFamily = any
Interface = ${vpn_name}
ConnectTo = ${server_name}
mode = switch 
Cipher = aes-256-cbc
Digest = SHA512

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc.conf"

} #config_client_batch()

#### UP AND DOWN CLIENT SCRIPTS ####

Darwin-up-down-client()
{
  cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-up
#!/bin/sh

ifconfig \$INTERFACE ${server_tinc_IP}
ifconfig \$INTERFACE mtu 1350

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-up"

cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-down
#!/bin/sh
ifconfig \$INTERFACE down
EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-down"
} 


FreeBSD-up-down-client()
{
cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-up
#!/bin/sh
VPN_GATEWAY=${server_tinc_IP}
IP_CLIENT=${client_IP}
IP_SERVER=${server_public_IP}
ORIGINAL_GATEWAY=\$\(netstat -rn | awk ' /^default/ {print \$2}'\)
ifconfig \$INTERFACE up
    
ifconfig \$INTERFACE \$IP_CLIENT
ip r a \$IP_SERVER via \$ORIGINAL_GATEWAY

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-up"

cat << EOI >> ${OS_PREFIX}/tinc/${vpn_name}/tinc-down
#!/bin/sh
IP_SERVER=${server_public_IP}
ORIGINAL_GATEWAY=\$\(ip route show | grep ^default | cut -d ' ' -f 3-5\)

ip addr flush dev \$INTERFACE
ip r d \$IP_SERVER via \$ORIGINAL_GATEWAY
ip link set \$INTERFACE down

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-down"

} # FreeBSD-up-down-client

Linux-up-down-client()
{
cat << EOI > ${OS_PREFIX}/tinc/${vpn_name}/tinc-up
#!/bin/sh
VPN_GATEWAY=${server_tinc_IP}
IP_CLIENT=${client_IP}/${prefix}
IP_SERVER=${server_public_IP}
ORIGINAL_GATEWAY=\$(ip route show | grep default | cut -d ' ' -f 3-5)
ip link set \$INTERFACE up
    
ip a a \$IP_CLIENT dev \$INTERFACE
ip r a \$IP_SERVER via \$ORIGINAL_GATEWAY

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-up"

cat << EOI >> ${OS_PREFIX}/tinc/${vpn_name}/tinc-down
#!/bin/sh
IP_SERVER=${server_public_IP}
ORIGINAL_GATEWAY=\$(ip route show | grep default | cut -d ' ' -f 3-5)

ip addr flush dev \$INTERFACE
ip r d \$IP_SERVER via \$ORIGINAL_GATEWAY
ip link set \$INTERFACE down

EOI
    print "${OS_PREFIX}/tinc/${vpn_name}/tinc-down"

} Linux-up-down-client

genfiles()
{
    print "[*] Creating directories and files..."
    mkdir -p "${OS_PREFIX}"/tinc/"${vpn_name}"/hosts
    touch "${OS_PREFIX}"/tinc/"${vpn_name}"/tinc{-up,-down,.conf}
    touch "${OS_PREFIX}"/tinc/"${vpn_name}"/hosts/"${client}"
}

### Metafunctions

conf_server()
{
    fetch_server_variables
    genfiles
    config_server_batch
    $(uname)-up-down-server;;
}

conf_client()
{
    fetch_client_variables
    genfiles
    config_client_batch
    $(uname)-up-down-client;;
    esac
}

select_conf()
{
    if [[ ${role} == "server" ]]
    then
        conf_server
    elif [[ ${role} == "client" ]]
    then
        conf_client
    else
        print -u 2 "Please specify a role : \"client\" or \"server\""
        exit 1
    fi
}

# Yes. 
export role="$1"
disp_banner
select_conf
print "[*] Key Generation"
tincd -n "${vpn_name}" --generate-keys=4096
chmod +x "${OS_PREFIX}"/tinc/"${vpn_name}"/tinc-*
print "[!] Finished"
